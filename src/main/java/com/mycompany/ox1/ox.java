/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.ox1;

import java.util.Scanner;

/**
 *
 * @author admin
 */
public class ox {

    static char turn = 'O';
    static String checkWin = "Draw";

    public static void main(String[] args) {
        Scanner kb = new Scanner(System.in);
        String[][] arr = {{"-", "-", "-"}, {"-", "-", "-"}, {"-", "-", "-"}};
        System.out.println("Welcome to Ox Game");
        char endGame = 0;

        while (endGame == 0) {
            if (CheckHorizontal(arr)) {
                endGame = 1;
                break;
            } else {
                if (CheckVertical(arr)) {
                    endGame = 1;
                    break;
                } else if (CheckLeftOblique(arr)) {
                    endGame = 1;
                    break;
                } else if (CheckRightOblique(arr)) {
                    endGame = 1;
                    break;
                } else if (CheckDraw(arr) == 9) {
                    PrintTable(arr);
                    endGame = 1;
                    break;
                }

            }
            if (turn == 'X') {
                PrintTable(arr);
                System.out.println("turn X");
                System.out.println("Please input row, col:");
                int x = kb.nextInt();
                int y = kb.nextInt();

                EditXTable(x, y, arr);

            } else {
                PrintTable(arr);
                System.out.println("turn O");
                System.out.println("Please input row, col:");
                int x = kb.nextInt();
                int y = kb.nextInt();

                EditOTable(x, y, arr);

            }
        }

        WinnerCheck();
    }

    private static void WinnerCheck() {
        if (checkWin.equals("X")) {
            System.out.println(">>>X Win<<<");
        } else if (checkWin.equals("O")) {
            System.out.println(">>>O Win<<<");
        } else {
            System.out.println(">>>Draw<<<");
        }
    }

    private static int CheckDraw(String[][] arr) {
        int count = 0;
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (!arr[i][j].equals("-")) {
                    count++;
                }
            }
        }
        return count;
    }

    private static boolean CheckRightOblique(String[][] arr) {
        if (arr[0][2].equals(arr[1][1]) && arr[1][1].equals(arr[2][0]) && !arr[0][2].equals("-")) {
            checkWin = arr[0][2];
            PrintTable(arr);
            return true;
        }
        return false;
    }

    private static boolean CheckLeftOblique(String[][] arr) {
        if (arr[0][0].equals(arr[1][1]) && arr[1][1].equals(arr[2][2]) && !arr[0][0].equals("-")) {
            checkWin = arr[0][0];
            PrintTable(arr);
            return true;
        }
        return false;
    }

    private static boolean CheckVertical(String[][] arr) {
        for (int i = 0; i < 3; i++) {
            if (arr[0][i].equals(arr[1][i]) && arr[1][i].equals(arr[2][i]) && !arr[0][i].equals("-")) {
                checkWin = arr[0][i];
                PrintTable(arr);
                return true;
            }
        }
        return false;
    }

    private static void EditOTable(int x, int y, String[][] arr) {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (x - 1 == i && y - 1 == j && arr[i][j].equals("-")) {
                    arr[i][j] = "O";
                    turn = 'X';
                    break;
                }
            }
        }
    }

    private static void EditXTable(int x, int y, String[][] arr) {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (x - 1 == i && y - 1 == j && arr[i][j].equals("-")) {
                    arr[i][j] = "X";
                    turn = 'O';
                    break;
                }
            }
        }
    }

    private static void PrintTable(String[][] arr) {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                System.out.print(arr[i][j] + " ");
            }
            System.out.println(" ");
        }
    }

    private static boolean CheckHorizontal(String[][] arr) {
        for (int i = 0; i < 3; i++) {
            if (arr[i][0].equals(arr[i][1]) && arr[i][1].equals(arr[i][2]) && !arr[i][0].equals("-")) {
                checkWin = arr[i][0];
                PrintTable(arr);
                return true;
            }
        }
        return false;
    }
}
